#ifndef ROVERDRIVER_H
#define ROVERDRIVER_H

class Rover;
class Terrain;
class Track;

class RoverDriver {
public:
    RoverDriver();
    RoverDriver Drive(Rover &rover);
    RoverDriver Over(Terrain &terrain);
	RoverDriver MapTo(Terrain &map);
	RoverDriver TrackTo(Track &track);
	int Now();

private:
    Rover *r;
    Terrain *t, *m;
	Track *plot;
};

#endif // ROVERDRIVER_H
