#ifndef ROVER_H
#define ROVER_H

#include <iostream>
#include <string>
#include <vector>

class Rover {
public:
    Rover();
    unsigned long GetX();
    unsigned long GetY();
    char GetOrientation();
	std::string GetDirections();
    unsigned long GetCurrentInstruction();
    unsigned long GetLastInstruction();

    void MoveStep();
	friend std::ostream& operator << (std::ostream &out, const Rover &t);
	friend std::istream& operator >> (std::istream &in, Rover &t);
private:
	unsigned long x, y;
	char orientation;
	std::string directions;
    unsigned long currentInstruction;
};

std::ostream& operator << (std::ostream &out, const std::vector<Rover> &t);
std::istream& operator >> (std::istream &in, std::vector<Rover> &v);

#endif // ROVER_H
