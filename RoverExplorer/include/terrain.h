#ifndef TERRAIN_H
#define TERRAIN_H

#include <iostream>
#include <vector>

class Terrain {
public:
    static const int UNMAPPED;

    Terrain(unsigned long x = 0, unsigned long y = 0);
    unsigned long GetWidth();
    unsigned long GetHeight();
    int GetTopografyAt(unsigned long x, unsigned long y);
    void SetTopografyAt(unsigned long x, unsigned long y, int topography);
	void PrintHeadsUp();
	friend std::ostream& operator << (std::ostream &out, const Terrain &t);
	friend std::istream& operator >> (std::istream &in, Terrain &t);

private:
	std::vector<std::vector<int> > aTopography;
};

#endif // TERRAIN_H
