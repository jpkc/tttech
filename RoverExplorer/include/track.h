#ifndef TRACK_H
#define TRACK_H

#include <vector>

class Track
{
public:
    void AddPoint(unsigned long x, unsigned long y, int topography);
    unsigned long GetX(unsigned long point);
    unsigned long GetY(unsigned long point);
    int GetTopography(unsigned long point);
	friend std::ostream& operator << (std::ostream &out, const Track &t);

private:
    std::vector<unsigned long> aX, aY;
    std::vector<int> aTopo;
};

#endif // TRACK_H
