#include <iostream>

using namespace std;

int main(int, char**) {
	int k;
	cout << "Rover zig-zag edge size: ";
	cin >> k;
	cout << "0 0 N" << endl;
	for(unsigned int j = 1; j < k; ++j) {
		for(unsigned int i = 1; i < k; ++i)
			cout << "M";
		(j%2 != 0) ? cout << "RMR" : cout << "LML";
	}
	for(unsigned int i = 1; i < k; ++i)
		cout << "M";
}
