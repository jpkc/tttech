#include <iostream>
#include <iomanip>

using namespace std;

int main(int, char **) {
	int x, y, k = 0;
	cin >> x >> y;
	cout << x << " " << y << endl;
	for(int j = 0; j <= y; ++j) {
		for(int i = 0; i <= x; ++i)
			cout << setw(5) << k++;
		cout << endl;
	}
}
