#define CATCH_CONFIG_MAIN
#include <iostream>
using namespace std;

#include "../tests/include/catch.hpp"
#include "../include/terrain.h"
#include "../include/rover.h"
#include "../include/track.h"
#include "../include/roverdriver.h"

TEST_CASE("Interface setters", "[RoverDriver]") {
	RoverDriver interface;
	Rover rover;
	Track track;
	Terrain mars, map;
	interface.Drive(rover);
	interface.Over(mars);
	interface.MapTo(map);
	interface.TrackTo(track);
}
TEST_CASE("Interface working", "[RoverDriver]") {
	RoverDriver interface;
	interface.Now();
}
