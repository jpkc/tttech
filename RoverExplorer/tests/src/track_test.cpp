#define CATCH_CONFIG_MAIN
#include <iostream>
using namespace std;

#include "../tests/include/catch.hpp"
#include "../include/track.h"

TEST_CASE("Track tracks properly", "[Track]") {
	Track t;
	t.AddPoint(0, 7, -47);
	t.AddPoint(1, 76, -471);
	t.AddPoint(12, 765, -472);
	t.AddPoint(123, 7654, -473);
	t.AddPoint(1234, 76543, -474);
	t.AddPoint(12345, 765432, -475);
	t.AddPoint(123456, 7654321, -476);
	REQUIRE(t.GetX(0) == 0);
	REQUIRE(t.GetY(0) == 7);
	REQUIRE(t.GetTopography(0) == -47);
	REQUIRE(t.GetX(3) == 123);
	REQUIRE(t.GetY(3) == 7654);
	REQUIRE(t.GetTopography(3) == -473);
	REQUIRE(t.GetX(6) == 123456);
	REQUIRE(t.GetY(6) == 7654321);
	REQUIRE(t.GetTopography(6) == -476);
}

TEST_CASE("Track outputs properly", "[Track]") {
	Track t;
	stringstream ss;
	ss << t;
}
