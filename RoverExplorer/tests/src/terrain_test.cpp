#define CATCH_CONFIG_MAIN
#include <iostream>

using namespace std;

#include "../tests/include/catch.hpp"
#include "../include/terrain.h"

TEST_CASE("Terrain constructor", "[Terrain]") {
    Terrain terrain(20, 20);
    REQUIRE(terrain.GetTopografyAt(0, 0) == Terrain::UNMAPPED);
    REQUIRE(terrain.GetTopografyAt(19, 0) == Terrain::UNMAPPED);
    REQUIRE(terrain.GetTopografyAt(0, 19) == Terrain::UNMAPPED);
    REQUIRE(terrain.GetTopografyAt(19, 19) == Terrain::UNMAPPED);
    REQUIRE(terrain.GetTopografyAt(10, 10) == Terrain::UNMAPPED);
}
TEST_CASE("Terrain topography setter and getter", "[Terrain]") {
    Terrain terrain(20, 20);
    terrain.SetTopografyAt(0, 0, 42);
    REQUIRE(terrain.GetTopografyAt(0, 0) == 42);
    REQUIRE(terrain.GetTopografyAt(20, 20) == 42);
    terrain.SetTopografyAt(10, 10, 27);
    REQUIRE(terrain.GetTopografyAt(10, 10) == 27);
    terrain.SetTopografyAt(127, 419, 13);
    REQUIRE(terrain.GetTopografyAt(127, 419) == 13);
}
TEST_CASE("Terrain inputs properly", "[Terrain]") {
    stringstream ss;
    Terrain terrain;
    ss << "7 5"
          "  1  2  3  4  5  6  7"
          "  8  9 10 11 12 13 14"
          " 15 16 17 18 19 20 21"
          " 22 23 24 25 26 27 28"
          " 29 30 31 32 33 34 35";
    ss >> terrain;
}
TEST_CASE("Terrain outputs properly", "[Terrain]") {
    stringstream ss_in, ss_out;
    Terrain terrain;
    ss_in << "7 3"
          "  1  2  3  4  5  6  7"
          "  8  9 10 11 12 13 14"
          " 15 16 17 18 19 20 21";
    ss_in >> terrain;
    ss_out << terrain;
}
TEST_CASE("Terrain reports topography properly", "[Terrain]") {
    stringstream ss;
    Terrain terrain;
    ss << "10 10"
           "  1   2   3   4   5   6   7   8   9  10"
           " 11  12  13  14  15  16  17  18  19  20"
           " 21  22  23  24  25  26  27  28  29  30"
           " 31  32  33  34  35  36  37  38  39  40"
           " 41  42  43  44  45  46  47  48  49  50"
           " 51  52  53  54  55  56  57  58  59  60"
           " 61  62  63  64  65  66  67  68  69  70"
           " 71  72  73  74  75  76  77  78  79  80"
           " 81  82  83  84  85  86  87  88  89  90"
           " 91  92  93  94  95  96  97  98  99 100";
    ss >> terrain;
    REQUIRE(terrain.GetTopografyAt(0, 0) == 1);
    REQUIRE(terrain.GetTopografyAt(9, 0) == 10);
    REQUIRE(terrain.GetTopografyAt(0, 9) == 91);
    REQUIRE(terrain.GetTopografyAt(9, 9) == 100);
    REQUIRE(terrain.GetTopografyAt(6, 7) == 77);
    REQUIRE(terrain.GetTopografyAt(10, 10) == terrain.GetTopografyAt(0, 0));
    REQUIRE(terrain.GetTopografyAt(5, 10) == terrain.GetTopografyAt(5, 0));
    REQUIRE(terrain.GetTopografyAt(10, 5) == terrain.GetTopografyAt(0, 5));
}
