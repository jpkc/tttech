#include <iostream>
#include "track.h"

void Track::AddPoint(unsigned long x, unsigned long y, int topography) {
	aX.push_back(x);
	aY.push_back(y);
	aTopo.push_back(topography);
}

unsigned long Track::GetX(unsigned long point) {
	return aX[point];
}

unsigned long Track::GetY(unsigned long point) {
	return aY[point];
}

int Track::GetTopography(unsigned long point) {
	return aTopo[point];
}

std::ostream& operator << (std::ostream &out, const Track &t) {
	out << t.aX.size();
	for(unsigned long i = 0; i < t.aX.size(); ++i)
		out << std::endl << t.aX[i] << " " << t.aY[i];
	return out;
}
