#include "terrain.h"
#include <iomanip>
#include <climits>

const int Terrain::UNMAPPED = INT_MIN;

Terrain::Terrain(unsigned long x, unsigned long y) {
    for(unsigned long j = 0; j < y; ++j)
		aTopography.push_back(std::vector<int>(x, INT_MIN));
}

unsigned long Terrain::GetWidth() {
    if(aTopography.size() > 0)
        return aTopography[0].size();
    return 0;
}

unsigned long Terrain::GetHeight() {
    return aTopography.size();
}

int Terrain::GetTopografyAt(unsigned long x, unsigned long y) {
    y%=aTopography.size();
    x%=aTopography[y].size();
    return aTopography[y][x];
}

void Terrain::SetTopografyAt(unsigned long x, unsigned long y, int topography) {
    if(aTopography.size() > 0) {
        y%=aTopography.size();
        if(aTopography[0].size() > 0) {
            x%=aTopography[0].size();
            aTopography[y][x] = topography;
        }
    }
}

void Terrain::PrintHeadsUp() {
	for(unsigned long j = aTopography.size(); j > 0; --j) {
		for(unsigned long i = 0; i < aTopography.size(); ++i) {
			if(aTopography[j-1][i] == Terrain::UNMAPPED)
				std::cout << " XXX";
			else
				std::cout << std::setw(4) << aTopography[j-1][i];
		}
		std::cout << std::endl;
	}
}

std::ostream& operator << (std::ostream &out, const Terrain &t) {
	if(t.aTopography.size() > 0)
		out << t.aTopography.size() - 1 << " " << t.aTopography[0].size() - 1;

	for(unsigned long j = 0; j < t.aTopography.size(); ++j) {
		out << std::endl;
		for(unsigned long i = 0; i < t.aTopography.size(); ++i) {
			out << " " << t.aTopography[j][i];
        }
    }
    return out;
}

std::istream& operator >> (std::istream &in, Terrain &t) {
    unsigned long x, y;
    int z;
	std::vector<int> strip;
	std::vector<std::vector<int> > terrain;
    in >> x >> y;
	x++;
	y++;
    terrain.clear();
    for(unsigned long j = 0; j < y; ++j){
        strip.clear();
        for(unsigned long i = 0; i < x; ++i){
            in >> z;
            strip.push_back(z);
        }
        terrain.push_back(strip);
    }
    t.aTopography.swap(terrain);
    return in;
}
