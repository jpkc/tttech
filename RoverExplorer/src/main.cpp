#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "terrain.h"
#include "rover.h"
#include "roverdriver.h"
#include "track.h"
//uncomment to drive rovers in parallel threads
//#include <unistd.h>
//#include <sys/wait.h>

using namespace std;

int main() {
	Terrain mars;
	vector<Rover> rover;
	vector<Track> track;
	Terrain map;
	RoverDriver interface;
//  uncomment to drive rovers in parallel threads
//	pid_t wpid;
//	int status = 0;

	cin >> mars;
	cin >> rover;
	map = Terrain(mars.GetWidth(), mars.GetHeight());
	for(unsigned long i = 0; i < rover.size(); ++i) {
		track.push_back(Track());
//      uncomment to drive rovers in parallel threads
//		pid_t pid = fork();
//		if (pid == 0) {
			for(unsigned long step = rover[i].GetCurrentInstruction(); step < rover[i].GetLastInstruction(); ++step)
				interface.Drive(rover[i]).Over(mars).MapTo(map).TrackTo(track[i]).Now();
			ofstream file;
			stringstream filename;
			filename << "rover" << i << ".txt";
			file.open(filename.str());
			file << track[i] << endl;
			file << rover[i] << endl;
			file.close();
//		    uncomment to drive rovers in parallel threads
//			exit(0);
//		}
	}
//  uncomment to drive rovers in parallel threads
//	while ((wpid = wait(&status)) > 0);
	ofstream file;
	file.open("mapping.txt");
	file << map << endl;
	file.close();

	return 0;
}
