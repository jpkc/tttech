#include "rover.h"

Rover::Rover() {
    currentInstruction = 0;
}

unsigned long Rover::GetX() {
    return x;
}

unsigned long Rover::GetY() {
    return y;
}

char Rover::GetOrientation() {
    return orientation;
}

std::string Rover::GetDirections() {
    return directions;
}

unsigned long Rover::GetCurrentInstruction() {
    return currentInstruction;
}

unsigned long Rover::GetLastInstruction() {
    return directions.size();
}

void Rover::MoveStep() {
    char instruction = directions[currentInstruction++];
    switch(instruction) {
    default:
        break;
    case 'l':
    case 'L':
        switch (orientation) {
        case 'n':
        case 'N':
            orientation = 'W';
            break;
        case 's':
        case 'S':
            orientation = 'E';
            break;
        case 'e':
        case 'E':
            orientation = 'N';
            break;
        case 'w':
        case 'W':
            orientation = 'S';
            break;
        }
        break;
    case 'r':
    case 'R':
        switch (orientation) {
        case 'n':
        case 'N':
            orientation = 'E';
            break;
        case 's':
        case 'S':
            orientation = 'W';
            break;
        case 'e':
        case 'E':
            orientation = 'S';
            break;
        case 'w':
        case 'W':
            orientation = 'N';
            break;
        }
        break;
    case 'm':
    case 'M':
        switch(orientation) {
        default:
            break;
        case 'n':
        case 'N':
            ++y;
            break;
        case 's':
        case 'S':
            --y;
            break;
        case 'e':
        case 'E':
            ++x;
            break;
        case 'w':
        case 'W':
            --x;
            break;
        }
        break;
    }
}

std::ostream& operator << (std::ostream &out, const Rover &r) {
	out << r.x << " " << r.y << " " << r.orientation;
    return out;
}

std::istream& operator >> (std::istream &in, Rover &r) {
    r.currentInstruction = 0;
    in >> r.x >> r.y >> r.orientation >> r.directions;
    return in;
}

std::ostream& operator << (std::ostream &out, const std::vector<Rover> &t) {
    for(unsigned long int i = 0; i < t.size(); ++i) {
		out << t[i] << std::endl;
    }
    return out;
}

std::istream& operator >> (std::istream &in, std::vector<Rover> &v) {
    while(1) {	//I'd rather specify the number of drones.
        Rover r;
        in >> r;
        if(in.eof())
            break;
        v.push_back(r);
    }
    return in;
}
