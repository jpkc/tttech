#include "roverdriver.h"
#include "rover.h"
#include "terrain.h"
#include "track.h"

RoverDriver::RoverDriver() {
	r = NULL;
	t = NULL;
	m = NULL;
	plot = NULL;
}

RoverDriver RoverDriver::Drive(Rover &rover) {
    r = &rover;
    return *this;
}

RoverDriver RoverDriver::Over(Terrain &terrain) {
    t = &terrain;
    return *this;
}

RoverDriver RoverDriver::MapTo(Terrain &map) {
    m = &map;
    return *this;
}

RoverDriver RoverDriver::TrackTo(Track &track) {
	plot = &track;
	return *this;
}

int RoverDriver::Now() {
	if(r)	{
		if(t) {
		if(plot)
			plot->AddPoint(r->GetX(), r->GetY(), t->GetTopografyAt(r->GetX(), r->GetY()));
		if(m)
			m->SetTopografyAt(r->GetX(), r->GetY(), t->GetTopografyAt(r->GetX(), r->GetY()));
		r->MoveStep();
		if(m)
			m->SetTopografyAt(r->GetX(), r->GetY(), t->GetTopografyAt(r->GetX(), r->GetY()));
		}
	}
    return 0;
}
